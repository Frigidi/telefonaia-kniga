﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoneBook
{
    [Serializable]
    class Contact
    {
        public string Fio { set; get; }
        public string Number { set; get; }
        public string Description { set; get; }
    }
}
