﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace PhoneBook
{
    public partial class Form1 : Form
    {
        private const string FileName = "contacts.db";
        private const int NothingSelected = -1;
        private List<Contact> contacts;
        private int selectedRowIndex;

        private void LoadContactsFromFile()
        {
            if(File.Exists(FileName) == true)
            {
                FileStream fs = new FileStream(FileName, FileMode.Open);

                BinaryFormatter bf = new BinaryFormatter();
                contacts = (List<Contact>)bf.Deserialize(fs);

                fs.Close();
            }
        }

        private void SaveContactsToFile()
        {
            FileStream fs = new FileStream(FileName, FileMode.OpenOrCreate);

            BinaryFormatter bf = new BinaryFormatter();
            bf.Serialize(fs, contacts);

            fs.Close();
        }

        private void RefreshDaraGridViewContacts()
        {
            dataGridViewContacts.Rows.Clear();

            for (int i = 0; i < contacts.Count; i++)
            {
                dataGridViewContacts.Rows.Add(
                    contacts[i].Fio,
                    contacts[i].Number,
                    contacts[i].Description
                    );
            }
        }

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            contacts = new List<Contact>();
            LoadContactsFromFile();
            RefreshDaraGridViewContacts();

            selectedRowIndex = NothingSelected;
            buttonChange.Left = buttonAdd.Left;
            buttonChange.Top = buttonAdd.Top;
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            contacts.Add(new Contact()
            {
                Fio = textBoxFIO.Text,
                Number = maskedTextBoxNumber.Text,
                Description = textBoxDescription.Text
            });

            RefreshDaraGridViewContacts();

            textBoxFIO.Clear();
            maskedTextBoxNumber.Clear();
            textBoxDescription.Clear();
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            SaveContactsToFile();
        }

        private void dataGridViewContacts_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            selectedRowIndex = e.RowIndex;
        }

        private void удалитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(selectedRowIndex != NothingSelected)
            {
                contacts.RemoveAt(selectedRowIndex);
                RefreshDaraGridViewContacts();
                selectedRowIndex = NothingSelected;
            }
        }

        private void редактироватьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(selectedRowIndex != NothingSelected)
            {
                textBoxFIO.Text = contacts[selectedRowIndex].Fio;
                maskedTextBoxNumber.Text = contacts[selectedRowIndex].Number;
                textBoxDescription.Text = contacts[selectedRowIndex].Description;

                buttonChange.Visible = true;
            }
        }

        private void buttonChange_Click(object sender, EventArgs e)
        {
            contacts[selectedRowIndex].Fio = textBoxFIO.Text;
            contacts[selectedRowIndex].Number = maskedTextBoxNumber.Text;
            contacts[selectedRowIndex].Description = textBoxDescription.Text;

            buttonChange.Visible = false;
            textBoxFIO.Clear();
            maskedTextBoxNumber.Clear();
            textBoxDescription.Clear();

            selectedRowIndex = NothingSelected;

            RefreshDaraGridViewContacts();
        }
    }
}
